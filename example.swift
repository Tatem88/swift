import Swift 

print("Hello World!")


let n = 5

if (n > 5) {
	print("Больше 5")
} else {
	print("Меньше или равно 5")
}

let animals = ["Dog", "Cat", "Cow", "Human"]
for animal in animals {
	print(animal)
}


for i in 0...100 {
	print(i)
}

let animal = "Cat"

func hiCat(animal: String) -> String {
    let sayHello = "Hello, " + animal + "!"
    return sayHello
}

print(hiCat(animal: "Dog"))


class Cat {
	var name: String
	var age: Int
	
	
	func myau() {
		print("Myaaau!")
	}
	
	init(name: String, age: Int){
		self.name = name
		self.age = age
	}
}

var catVasya = Cat(name: "Vasya", age: 3)

catVasya.myau()